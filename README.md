# GOVIPS

Docker container for building go binaries dependent on LibVips.  This container contains a compiled libvips binaries
in /opt/vips 

# Example

```docker

RUN mkdir /app && \
    mkdir /BUILD

FROM rsvancara:go-vips-builder  as builder

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y wget curl ca-certificates \
    pkg-config libexpat1 \
    libglib2.0 fftw3 liblcms2 libexif libjpeg libwebp


# Build the binary
COPY cmd /BUILD/cmd
COPY go.sum  /BUILD.go.sum
COPY go.mod /BUILD/go.mod
COPY vendor /BUILD/vendor
COPY internal /BUILD/internal
RUN cd /BUILD && PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/vips/lib/pkgconfig LD_LIBRARY_PATH=/opt/vips/lib /usr/local/go/bin/go build -o /BUILD/dabloog cmd/goblog/main.go 

```


# Container Runtime Libraries

Don't forget to include runtime libraries for your libvips and 

```docker

COPY --from=builder /opt/vips /opt/vips

RUN \
  apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y libjpeg62 libexpat1 libglib2.0-0 libfftw3-3 liblcms2-2 libexif12 ca-certificates && \
  apt-get clean

```

